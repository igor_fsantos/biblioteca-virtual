<?php

/*** comeca a sessao ***/
session_start();

$loginUser = filter_var($_POST['loginUser'], FILTER_SANITIZE_STRING);
$senhaUser = filter_var($_POST['senhaUser'], FILTER_SANITIZE_STRING);
?>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Devolucao Livro</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">  
  </head>
  <body>
    <script type="text/javascript">
      function submeteForm(){
        document.getElementById("cancelar").submit();
      }
    </script>
    <div class="form">
      <h2>Devolucao do Livro</h2>
      <form action="login_submissao.php" id="cancelar" method="post"/>
        <input type="hidden" id="login" name="login" value=<?php echo $loginUser; ?> maxlength="20" />
        <input type="hidden" id="senha" name="senha" value=<?php echo $senhaUser; ?> maxlength="20" />
      </form>
      <form action="devolve_livro_submissao.php" method="post">
        <div class="tab-content">
          <fieldset>
            <div class="field-wrap">
             <label for="matriculaEstudante">
               Numero de Matricula<span class="req">*</span>
             </label>
             <input type="number" autocomplete="off" id="matriculaEstudante" name="matriculaEstudante" value="" min="1" onkeypress="return isNumberKey(event)" required/>
            </div> <!-- field-wrap -->
            <div class="field-wrap">
             <label for="isbn">
              ISBN do Livro<span class="req">*</span>
             </label>
             <input type="text" autocomplete="off" id="isbn" name="isbn" value="" maxlength="15" required/>
            </div> <!-- field-wrap -->
            <div class="top-row">
              <div class="field-wrap">
                <button type="button" class="button button-block" onclick="submeteForm()"/>Cancelar</button>
              </div> <!-- field-wrap -->
              <div class="field-wrap">
                <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
                <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
                <button type="submit" class="button button-block"/>Devolucao</button>
              </div> <!-- field-wrap -->
            </div> <!-- top-row-->
          </fieldset>
        </div> <!-- tab-content -->
      </form>
    </div> <!-- form -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>
  </body>
</html>