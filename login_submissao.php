<?php

/*** Comeca a sessao ***/
session_start();

$message = '';
$url = '';

/*** verifica se o usuario ja esta logado ***/
/*if(isset( $_SESSION['login_usuario'] ))
{
    $message = 'O usuario ja esta logado.';
}*/
/*** verifica se o login e a senha foram submetidos. ***/
if(!isset( $_POST['login'], $_POST['senha']))
{
  $message = 'Por favor, entre com um login e uma senha que sejam validos';
}
/*** verifica se o login esta com um tamanho valido ***/
elseif (strlen( $_POST['login']) > 20 || strlen($_POST['login']) < 4)
{
  $message = 'Tamanho invalido para o login.';
}
/*** verifica se a senha esta com um tamanho valido ***/
elseif (strlen( $_POST['senha']) > 20 || strlen($_POST['senha']) < 4)
{
  $message = 'Tamanho invalido para a senha.';
}
/*** verifica se o login contem apenas caracteres alfanumericos ***/
elseif (ctype_alnum($_POST['login']) != true)
{
  /*** Login invalido ***/
  $message = "O login deve ser composto somente por caracteres alfanumericos.";
}
/*** verifica se a senha e composta somente por caracteres alfanumericos ***/
elseif (ctype_alnum($_POST['senha']) != true)
{
  /*** Senha invalida***/
  $message = "A senha deve ser composta somente por caracteres alfanumericos.";
}
else
{
  /*** Se estamos aqui, os dados sao validos e podemos inseri-los no banco de dados ***/
  $login = filter_var($_POST['login'], FILTER_SANITIZE_STRING);
  $senhaPrev = filter_var($_POST['senha'], FILTER_SANITIZE_STRING);
  $senha = $senhaPrev;

  /*** agora podemos encriptar a senha ***/
  $senha = sha1( $senha );

  /*** conecta ao banco de dados ***/
  /*** mysql hostname ***/
  $mysql_hostname = 'localhost';

  /*** usuario do mysql ***/
  $mysql_username = 'root';

  /*** senha do mysql ***/
  $mysql_password = '';

  /*** nome do banco de dados ***/
  $mysql_dbname = 'BimManager';

    try
    {
      $dbh = new PDO("mysql:host=$mysql_hostname;dbname=$mysql_dbname", $mysql_username, $mysql_password);
      /*** $message = uma mensagem dizendo que conectou ***/

      /*** configura o modo de erros para excecoes ***/
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      /*** prepara a consulta ao banco de dados ***/
      $stmt = $dbh->prepare("SELECT login, senha, tipoUsuario FROM usuario 
        WHERE login = :login AND senha = :senha");

      /*** configura os parametros da consulta ***/
      $stmt->bindParam(':login', $login, PDO::PARAM_STR);
      $stmt->bindParam(':senha', $senha, PDO::PARAM_STR, 40);

      /*** executa a consulta que foi preparada ***/
      $stmt->execute();

      /*** busca por um resultado ***/
      $login_usuario = $stmt->fetchColumn();

      /*** executa a consulta que foi preparada ***/
      $stmt->execute();

        $tipoUsuario = $stmt->fetchColumn(2); // o tipo de usuario fica na 3ª coluna da tabela usuarios

        /*** se nao encontramos algum resultado, houve algum problema durante o procedimento ***/
        if($login_usuario == false)
        {
          $message = 'O login falhou.';
        }
        /*** se encontramos um resultado, entao ocorreu tudo bem ***/
        else
        {
          /*** configura a variavel de login do usuario ***/
          $_SESSION['login_usuario'] = $login_usuario;

          /*** busca o nome na tabela adequado para cada tipo de usuario ***/
          switch ($tipoUsuario) {
            case 'admin':
              $url = "http://localhost/login/funcoes_admin.php";
            break;

            case 'estudante':
              $url = "http://localhost/login/funcoes_estudante.php";
            break;
          }
        }


    }
    catch(Exception $e)
    {
      /*** Se estamos aqui, houve algum problema ao acessar o banco de dados ***/
      $message = 'Nao foi possivel processar a sua requisicao. Tente mais tarde novamente."';
    }

}
?>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Login Submetido</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css"> 
  </head>
  <body>
    <script type="text/javascript">
      var verif_url = '<?php echo $url; ?>';
      if(verif_url != ''){
        var f = document.createElement("form");
        f.setAttribute('method',"post");
        f.setAttribute('id', "myForm");
        f.setAttribute('action', verif_url);

        var l = document.createElement("input");
        l.setAttribute('type',"hidden");
        l.setAttribute('name',"loginUser");
        l.setAttribute('id', "loginUser");
        l.setAttribute('value', '<?php echo $login; ?>');

        var s = document.createElement("input");
        s.setAttribute('type', "hidden");
        s.setAttribute('name', "senhaUser");
        s.setAttribute('id', "senhUser");
        s.setAttribute('value', '<?php echo $senhaPrev; ?>');

        f.appendChild(l);
        f.appendChild(s);

        document.getElementsByTagName('body')[0].appendChild(f);

        document.getElementById("myForm").submit();
      }
    </script>
    <div class="form">
      <div class="tab-content">
        <h2><p><?php echo $message; ?></p></h2>
      </div> <!-- tab-content -->
      <div class="tab-content">
        <form action="logout.php" method="post"/>
          <button type="submit" class="button button-block"/>Sair</button>
        </form>
      </div> <!-- tab-content -->
    </div> <!-- /form -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>
  </body>
</html>

