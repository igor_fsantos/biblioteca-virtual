<?php
// Comeca a sessao.
session_start();

// Desaloca todas as variaveis da sessao.
session_unset();

// Destroi a sessao.
session_destroy();
?>
<html>
<head>
<title>Logged Out</title>
</head>
<body>
<script>
function mudarPagina() { // Funcao que troca de pagina
    location.replace("http://localhost/login/")
}
mudarPagina();
</script>
</body>
</html>