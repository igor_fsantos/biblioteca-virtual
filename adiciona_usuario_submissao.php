<html>
  <head>
    <meta charset="UTF-8">
    <title>Login Submetido</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css"> 
  </head>
  <body>

<?php
/*** comeca a sessao ***/
session_start();

$loginUser = filter_var($_POST['loginUser'], FILTER_SANITIZE_STRING);
$senhaUser = filter_var($_POST['senhaUser'], FILTER_SANITIZE_STRING);

/*** verifica se os dados foram submetidos corretamente ***/
if(!isset( $_POST['login'], $_POST['senha'], $_POST['tipoUsuario'], $_POST['form_token'], $_POST['nome'], $_POST['dataNascimento'], $_POST['cpf']))
{
  $message = 'Dados invalidos. Por favor, insira novamente os dados.';
}
/*** verifica se a form token e' valida ***/
elseif( $_POST['form_token'] != $_SESSION['form_token'])
{
  $message = 'Submissao de form token invalida.';
}
/*** verifica se o login tem o tamanho valido ***/
elseif (strlen( $_POST['login']) > 16 || strlen($_POST['login']) < 1)
{
  $message = 'Tamanho invalido para o login.';
}
/*** verifica se a senha tem o tamanho valido ***/
elseif (strlen( $_POST['senha']) > 20 || strlen($_POST['senha']) < 4)
{
  $message = 'Tamanho invalido para a senha.';
}
/*** verifica se o login contem apenas caracteres alfanumericos ***/
elseif (ctype_alnum($_POST['login']) != true)
{
  /*** login invalido ***/
  $message = "O login deve conter apenas caracteres alfanumericos";
}
/*** verifica se a senha contem apenas caracteres alfanumericos  ***/
elseif (ctype_alnum($_POST['senha']) != true)
{
  /*** senha invalida ***/
  $message = "A senha deve conter apenas caracteres alfanumericos";
}
else
{
  /*** se estamos aqui, os dados sao validos e podemos inseri-los no banco de dados ***/
  $login = filter_var($_POST['login'], FILTER_SANITIZE_STRING);
  $senha = filter_var($_POST['senha'], FILTER_SANITIZE_STRING);
  $tipoUsuario = filter_var($_POST['tipoUsuario'], FILTER_SANITIZE_STRING);
  $nome = filter_var($_POST['nome'], FILTER_SANITIZE_STRING);
  $dataNascimento = filter_var($_POST['dataNascimento'], FILTER_SANITIZE_STRING);
  $sexo = filter_var($_POST['sexo'], FILTER_SANITIZE_STRING);
  $cpf = filter_var($_POST['cpf'], FILTER_SANITIZE_STRING);
  $email = filter_var($_POST['email'], FILTER_SANITIZE_STRING);

  /*** agora podemos encriptar a senha***/
  $senha = sha1( $senha );

  /*** conecta ao banco de dados ***/
  /*** mysql hostname ***/
  $mysql_hostname = 'localhost';

  /*** mysql username ***/
  $mysql_username = 'root';

  /*** mysql password ***/
  $mysql_password = '';

  /*** database name ***/
  $mysql_dbname = 'BimManager';

  try
  {
    $dbh = new PDO("mysql:host=$mysql_hostname;dbname=$mysql_dbname", $mysql_username, $mysql_password);
    /*** $message = uma mensagem dizendo que conectamos ***/

    /*** configura o modo de erro para excecoes ***/
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    /*** prepara a insercao ***/
    $stmt = $dbh->prepare("INSERT INTO usuario( login, senha, tipoUsuario ) VALUES (:login, :senha, :tipoUsuario )");

    /*** configura os parametros ***/
    $stmt->bindParam(':login', $login, PDO::PARAM_STR);
    $stmt->bindParam(':senha', $senha, PDO::PARAM_STR);
    $stmt->bindParam(':tipoUsuario', $tipoUsuario, PDO::PARAM_STR);

    /*** executa a insercao com os parametros preparados ***/
    $stmt->execute();

    switch ($tipoUsuario) {
      case 'estudante':
      $stmt = $dbh->prepare("INSERT INTO dadosEstudante(nome, dataNascimento, email, CPF, sexo, numMatricula) VALUES(:nome, :dataNascimento, :email, :cpf, :sexo, :numMatricula)");
      $stmt->bindParam(':numMatricula', $login, PDO::PARAM_STR);
      break;

      case 'admin':
      $stmt = $dbh->prepare("INSERT INTO dadosAdministrador(idAdmin, nome, dataNascimento, CPF, email, sexo) VALUES(:idAdmin, :nome, :dataNascimento, :cpf, :email, :sexo)");
      $stmt->bindParam(':idAdmin', $login, PDO::PARAM_STR);
      break;
    }

    $stmt->bindParam(':nome', $nome, PDO::PARAM_STR);
    $stmt->bindParam(':dataNascimento', $dataNascimento, PDO::PARAM_STR);
    $stmt->bindParam(':email', $email, PDO::PARAM_STR);
    $stmt->bindParam(':cpf', $cpf, PDO::PARAM_STR);
    $stmt->bindParam(':sexo', $sexo, PDO::PARAM_STR);

    $stmt->execute();

    $message = "Novo usuario adicionado com sucesso!";

    /*** desaloca a form token ***/
    unset( $_SESSION['form_token'] );

  }
  catch(Exception $e)
  {
    /*** verifica se o login ja existe ***/
    if( $e->getCode() == 23000)
    {
      $message = 'Este login ja existe.';
    }
    else
    {
      /*** se estamos aqui, ocorreu algo de errado ao acessar o banco de dados ***/
      $message = 'Nao foi possivel processar a sua requisicao. Tente novamente mais tarde.';
    }

  }
}
?>
    <div class="form">
      <h1><p><?php echo $message; ?></p></h1>
        <form action="adiciona_usuario.php" method="post"/>
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Adicionar Um Novo Usuario</button>
        </form>
        <form action="login_submissao.php" method="post"/>
          <input type="hidden" id="login" name="login" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senha" name="senha" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Pagina de Funcoes</button>
        </form>
    </div> <!-- form -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>
  </body>
</html>