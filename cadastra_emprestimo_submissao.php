<html>
  <head>
    <meta charset="UTF-8">
    <title>Emprestimo Submetido</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css"> 
  </head>
  <body>

<?php

/*** comeca a sessao ***/
session_start();

$loginUser = filter_var($_POST['loginUser'], FILTER_SANITIZE_STRING);
$senhaUser = filter_var($_POST['senhaUser'], FILTER_SANITIZE_STRING);

/*** verifica se os dados foram submetidos corretamente ***/
if(!isset( $_POST['matriculaEstudante'], $_POST['isbn']))
{
  $message = 'Os dados nao foram preenchidos corretamente. Por favor, insira novamente os dados.';
}
else
{
  /*** se estamos aqui, os dados foram inseridos corretamente ***/
  $matriculaEstudante = filter_var($_POST['matriculaEstudante'], FILTER_SANITIZE_STRING);
  $isbn = filter_var($_POST['isbn'], FILTER_SANITIZE_STRING);

  /*** conecta ao banco de dados ***/
  /*** mysql hostname ***/
  $mysql_hostname = 'localhost';

  /*** mysql username ***/
  $mysql_username = 'root';

  /*** mysql password ***/
  $mysql_password = '';

  /*** database name ***/
  $mysql_dbname = 'BimManager';

  try
  {
    $dbh = new PDO("mysql:host=$mysql_hostname;dbname=$mysql_dbname", $mysql_username, $mysql_password);
    /*** $message = uma mensagem dizendo que conectamos ***/

    /*** configura o modo de erro para excecoes ***/
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    /*** prepara a busca ***/
    $stmt = $dbh->prepare("SELECT ISBN, quantidade FROM livro WHERE ISBN=:isbn");

    /*** configura os parametros ***/
    $stmt->bindParam(':isbn', $isbn, PDO::PARAM_STR);

    /*** executa a busca com os parametros preparados ***/
    $stmt->execute();

    $resultado = $stmt->fetch();

    if($resultado == false){
      $message = 'O ISBN do livro informado nao existe no banco de dados!';
    }else if($resultado['quantidade'] <= 0){
      $message = 'Este livro nao esta disponivel para o emprestimo!';
    }
    else{
      /*** prepara a busca ***/
      $stmt = $dbh->prepare("SELECT numMatricula FROM dadosEstudante WHERE numMatricula=:matriculaEstudante");

      /*** configura os parametros ***/
      $stmt->bindParam(':matriculaEstudante', $matriculaEstudante, PDO::PARAM_STR);

      /*** executa a busca com os parametros preparados ***/
      $stmt->execute();

      $verif_matricula = $stmt->fetchColumn();

      if($verif_matricula == false){
        $message = 'O numero de matricula informado nao existe no banco de dados!';
      }
      else{
        // apos todas essas verificacoes, os dados podem ser inseridos na tabela de emprestimo de livros
        /*** prepara a insercao***/
        $stmt = $dbh->prepare("INSERT INTO emprestimoLivro(matriculaEstudante, ISBN, dataEmprestimo, dataDevolucao) VALUES(:matriculaEstudante, :isbn, NULL, '')");

        /*** configura os parametros ***/
        $stmt->bindParam(':matriculaEstudante', $matriculaEstudante, PDO::PARAM_STR);
        $stmt->bindParam(':isbn', $isbn, PDO::PARAM_STR);

        $stmt->execute();

        /*** a data de devolucao do livro deve ser configurada ***/
        $stmt = $dbh->prepare("UPDATE emprestimoLivro SET dataDevolucao=DATE_ADD(dataEmprestimo, INTERVAL 7 DAY)");

        $stmt->execute();

        $message = 'Emprestimo de livro registrado com sucesso!';
      }

    }

  }
  catch(Exception $e)
  {
    /*** se estamos aqui, ocorreu algo de errado ao acessar o banco de dados ***/
    $message = 'Nao foi possivel processar a sua requisicao. Tente novamente mais tarde.';

  }
}

?>
    <div class="form">
      <h1><p><?php echo $message; ?></p></h1>
        <form action="cadastra_emprestimo.php" method="post"/>
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Cadastrar Novo Emprestimo</button>
        </form>
        <form action="login_submissao.php" method="post"/>
          <input type="hidden" id="login" name="login" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senha" name="senha" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Pagina de Funcoes</button>
        </form>
    </div> <!-- form -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>
  </body>
</html>