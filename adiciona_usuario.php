<?php

/*** comeca a sessao ***/
session_start();

$loginUser = filter_var($_POST['loginUser'], FILTER_SANITIZE_STRING);
$senhaUser = filter_var($_POST['senhaUser'], FILTER_SANITIZE_STRING);

/*** cria uma chave ***/
$form_token = md5( uniqid('auth', true) );

/*** define a chave da sessao ***/
$_SESSION['form_token'] = $form_token;
?>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Adicionar Usuario</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">    
  </head>
  <body>
    <script type="text/javascript">
      function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if(charCode > 31 && (charCode < 48 || charCode > 57))
          return false;
        return true;
      }
    </script>
    <script type="text/javascript">
      function submeteForm(){
        document.getElementById("cancelar").submit();
      }
    </script>
    <div class="form">
      <h1>Adicionar Usuario</h1>
      <form action="login_submissao.php" id="cancelar" method="post"/>
        <input type="hidden" id="login" name="login" value=<?php echo $loginUser; ?> maxlength="20" />
        <input type="hidden" id="senha" name="senha" value=<?php echo $senhaUser; ?> maxlength="20" />
      </form>
      <form action="adiciona_usuario_submissao.php" method="post"/>
        <div class="tab-content">
          <fieldset>
            <div class="field-wrap">
             <label for="loginSel">
               Login<span class="req">*</span>
             </label>
             <input type="number" autocomplete="off" id="loginSel" name="loginSel" value="" min="1" onkeypress="return isNumberKey(event)" required/>
            </div> <!-- field-wrap -->
            <div class="field-wrap">
             <label for="senha">
              Senha<span class="req">*</span>
             </label>
             <input type="password" autocomplete="off" id="senha" name="senha" value="" maxlength="32" required/>
            </div> <!-- field-wrap -->
            <div class="field-wrap">
              <label for="nome">
                Nome<span class="req">*</span>
              </label>
              <input type="text" autocomplete="off" id="nome" name="nome" value="" maxlength="127" required/>
            </div> <!-- field-wrap -->
            <div class="top-row">
              <div class="field-wrap">
                <label for="sexo">
                  Sexo
                </label>
                <br>
              </div> <!-- field-wrap -->
              <div class="field-wrap">
                <select name="sexo" id="sexo">
                  <option value="M">Masculino</option>
                  <option value="F">Feminino</option>
                </select>
              </div> <!-- field-wrap -->
            </div> <!-- top-row -->
            <div class="field-wrap">
              <label for="dataNascimento">
                Data de Nascimento<span class="req">*</span>
              </label>
              <input type="date" autocomplete="off" id="dataNascimento" name="dataNascimento" required/>
            </div> <!-- field-wrap -->
            <div class="field-wrap">
              <label for="cpf">
                CPF<span class="req">*</span>
              </label>
              <input type="number" autocomplete="off" id="cpf" name="cpf" value="" min="10000000000" max="99999999999" onkeypress="return isNumberKey(event)" required/>
            </div> <!-- field-wrap -->
            <div class="field-wrap">
              <label for="email">
                Email<span class="req"></span>
              </label>
              <input type="email" autocomplete="off" id="email" name="email" value="" maxlength="127"/>
            </div> <!-- field-wrap -->
            <div class="top-row">
              <div class="field-wrap">
                <label for="tipoUsuario">
                  Tipo de Usuario
                </label>
                <br>
              </div> <!-- field-wrap -->
              <div class="field-wrap">
                <select name="tipoUsuario" onchange="mudarCampos(this)">
                  <script type="text/javascript">
                    function mudarCampos(tipoU) {
                      var usuario = tipoU.value;
                      if(usuario == 'estudante') {
                        var inpLogin = document.getElementById("loginSel");
                        window.alert("estudante");
                        inpLogin.setAttribute('type', 'number');
                        inpLogin.setAttribute('onkeypress', "return isNumberKey(event)");
                        inpLogin.min = '1';
                      } else {
                        var inpLogin = document.getElementById("loginSel");
                        inpLogin.setAttribute('type', 'text');
                        inpLogin.removeAttribute('onkeypress');
                        inpLogin.maxlength = '32';
                      }
                    }
                  </script>
                  <option value="estudante">Estudante</option>
                  <option value="admin">Administrador</option>
                </select>
              </div> <!-- field-wrap -->
            </div> <!-- top-row -->
            <div class="top-row">
              <div class="field-wrap">
                <button type="button" class="button button-block" onclick="submeteForm()"/>Cancelar</button>
              </div> <!-- field-wrap -->
              <div class="field-wrap">
                <input type="hidden" name="form_token" value="<?php echo $form_token; ?>" />
                <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
                <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
                <button type="submit" class="button button-block"/>Adicionar</button>
              </div> <!-- field-wrap -->
            </div> <!-- top-row -->
          </fieldset>
        </div> <!-- tab-content -->
      </form>
    </div> <!-- /form -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>
  </body>
</html>