<html>
  <head>
  <meta charset="UTF-8">
  <title>Submete Remocao de Usuario</title>
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/style.css"> 
  </head>
  <body>
<?php
/*** comeca a sessao ***/
session_start();

$message = '';

$loginUser = filter_var($_POST['loginUser'], FILTER_SANITIZE_STRING);
$senhaUser = filter_var($_POST['senhaUser'], FILTER_SANITIZE_STRING);

/*** verifica se os dados foram submetidos corretamente ***/
if(!isset( $_POST['login']))
{
    $message = 'Por favor, preencha os campos corretamente.';
}
else{    
  /*** conecta ao banco de dados ***/
  /*** mysql hostname ***/
  $mysql_hostname = 'localhost';

  /*** mysql username ***/
  $mysql_username = 'root';

  /*** mysql password ***/
  $mysql_password = '';

  /*** database name ***/
  $mysql_dbname = 'BimManager';

  try{
    /*** se estamos aqui, os dados sao validos e podemos acessa-los no banco de dados ***/
    $login = filter_var($_POST['login'], FILTER_SANITIZE_STRING);

    $dbh = new PDO("mysql:host=$mysql_hostname;dbname=$mysql_dbname", $mysql_username, $mysql_password);
    /*** $message = uma mensagem dizendo que conectamos ***/

    /*** configura o modo de erro para excecoes ***/
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    /*** prepara a busca ***/
    $stmt = $dbh->prepare("SELECT tipoUsuario FROM usuario WHERE login=:login");

    /*** configura os parametros ***/
    $stmt->bindParam(':login', $login, PDO::PARAM_STR);

    /*** executa a consulta com os parametros preparados ***/
    $stmt->execute();

    $tipoUsuario = $stmt->fetchColumn();

    if($tipoUsuario == false){
      $message = "Usuario nao encontrado!";
    }else{
      /*** removendo os dados do estudante ou do administrador ***/
      switch ($tipoUsuario) {
        case 'estudante':
        	$stmt = $dbh->prepare("DELETE FROM dadosEstudante WHERE numMatricula=:login");
        	break;
        		
        case 'admin':
        	$stmt = $dbh->prepare("DELETE FROM dadosAdministrador WHERE idAdmin=:login");
        	break;
      }

      /*** configura os parametros ***/
      $stmt->bindParam(':login', $login, PDO::PARAM_STR);

      /*** executa a remocao com os parametros preparados ***/
      $stmt->execute();

      /*** removendo o usuario da tabela de usuarios ***/
      /*** prepara a remocao ***/
      $stmt = $dbh->prepare("DELETE FROM usuario WHERE login=:login");

      /*** configura os parametros ***/
      $stmt->bindParam(':login', $login, PDO::PARAM_STR);

      /*** executa a remocao com os parametros preparados ***/
      $stmt->execute();

      $message = "Usuario removido com sucesso!";
    }
  }
  catch(Exception $e){
      /*** se estamos aqui, ocorreu algo de errado ao acessar o banco de dados ***/
      $message = 'Nao foi possivel processar a sua requisicao. Tente novamente mais tarde.';
  }
}
?>
    <div class="form">
      <h2><p><?php echo $message; ?></p></h2>
      <div class="field-wrap">
        <form action="remove_usuario.php" method="post"> 
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Remover Usuario</button>
        </form>
        <form action="login_submissao.php" method="post"> 
          <input type="hidden" id="login" name="login" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senha" name="senha" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Pagina de Funcoes</button>
        </form>
      </div> <!-- field-wrap -->
    </div> <!-- form -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>
  </body>
</html>