<html>
	<head>
		<meta charset="UTF-8">
		<title>BimManager Login</title>
   	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
   	<link rel="stylesheet" href="css/normalize.css">
   	<link rel="stylesheet" href="css/style.css">    
	</head>
	<body>
		<div class="form">
			<h2>Logar</h2>
			<form action="login_submissao.php" method="post">
				<div class="tab-content">
					<fieldset>
						<h1>BimManager</h1>
						<div class="field-wrap">
							<label for="login">
								Login<span class="req">*</span>
							</label>
							<input type="text" autocomplete="off" id="login" name="login" value="" maxlength="20" />
						</div> <!-- field-wrap -->
						<div class="field-wrap">
							<label for="senha">
								Senha<span class="req">*</span>		
							</label>
							<input type="password" autocomplete="off" id="senha" name="senha" value="" maxlength="20" />
						</div> <!-- field-wrap -->
						<button type="submit" class="button button-block"/>Log In</button>
					</fieldset>
				</div> <!-- tab-content -->
			</form>
		</div> <!-- /form -->
		<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>
	</body>
</html>