<?php
/*** comeca a sessao ***/
session_start();

$loginUser = filter_var($_POST['loginUser'], FILTER_SANITIZE_STRING);
$senhaUser = filter_var($_POST['senhaUser'], FILTER_SANITIZE_STRING);

/*** conecta ao banco de dados ***/
/*** mysql hostname ***/
$mysql_hostname = 'localhost';

/*** mysql username ***/
$mysql_username = 'root';

/*** mysql password ***/
$mysql_password = '';

/*** database name ***/
$mysql_dbname = 'BimManager';

try
{
  $dbh = new PDO("mysql:host=$mysql_hostname;dbname=$mysql_dbname", $mysql_username, $mysql_password);
  /*** $message = uma mensagem dizendo que conectamos ***/

  /*** configura o modo de erro para excecoes ***/
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


  /*** prepara a a busca ***/
  $stmt = $dbh->prepare("SELECT nome FROM dadosAdministrador WHERE idAdmin = :loginUser");

  /*** configura os parametros ***/
  $stmt->bindParam(':loginUser', $loginUser, PDO::PARAM_STR);

  /*** executa a busca com os parametros preparados ***/
  $stmt->execute();

  /*** captura o nome do usuario ***/
  $nomeUsuario = $stmt->fetchColumn();

  if($nomeUsuario == false){
    $message = "Bem-Vindo !";
  }
  else{
    $message = "Bem-Vindo, ".$nomeUsuario." !"; 
  }
}
catch(Exception $e)
{
  /*** se estamos aqui, ocorreu algo de errado ao acessar o banco de dados ***/
  $message = 'Erro ao acessar o banco de dados.';
}
?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Funcoes do Administrador</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css"> 
  </head>
  <body>
    <div class="form">
      <div class="tab-content">
        <h1><p><?php echo $message; ?></p></h1>
        <form action="adiciona_usuario.php" method="post"> 
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Adicionar Um Novo Usuario</button>
        </form>
        <form action="lista_estudantes.php" method="post"> 
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Listar Estudantes</button>
        </form>
        <form action="lista_administradores.php" method="post"> 
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Listar Administradores</button>
        </form>
        <form action="remove_usuario.php" method="post"> 
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Remover Um Usuario</button>
        </form>
        <form action="cadastra_emprestimo.php" method="post"> 
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Cadastrar Emprestimo</button>
        </form>
        <form action="lista_emprestimo.php" method="post"> 
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Listar Emprestimos</button>
        </form>
        <form action="devolve_livro.php" method="post"> 
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Devolver Livro</button>
        </form>
        <form action="adiciona_loc.php" method="post"> 
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Adicionar Ala e Estante</button>
        </form>
        <form action="lista_loc.php" method="post"> 
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Listar Alas e Estantes</button>
        </form>
        <form action="remove_ala.php" method="post"> 
          <input type="hidden" id="loginUser" name="loginUser" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senhaUser" name="senhaUser" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Remover Alas</button>
        </form>
        <form action="logout.php" method="post"> 
          <button type="submit" class="button button-block"/>Log Out</button>
        </form>
      </div> <!-- tab-content -->
    </div> <!-- /form -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>
  </body>
</html>