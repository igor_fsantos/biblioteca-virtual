<html>
  <head>
    <title>Lista Emprestimos</title>
    <meta charset="UTF-8">
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">   
  </head>
  <body>
  <div class="formTable">

<?php
/*** comeca a sessao ***/
session_start();

// Capturando o login e a senha do usuario que esta conectado
$loginUser = filter_var($_POST['loginUser'], FILTER_SANITIZE_STRING);
$senhaUser = filter_var($_POST['senhaUser'], FILTER_SANITIZE_STRING);
$matriculaEstudante = filter_var($_POST['matriculaEstudante'], FILTER_SANITIZE_STRING);

/*** conecta ao banco de dados ***/
 /*** mysql hostname ***/
$mysql_hostname = 'localhost';

 /*** mysql username ***/
$mysql_username = 'root';

/*** mysql password ***/
$mysql_password = '';

 /*** database name ***/
$mysql_dbname = 'BimManager';

try{

  $dbh = new PDO("mysql:host=$mysql_hostname;dbname=$mysql_dbname", $mysql_username, $mysql_password);
  /*** $message = uma mensagem dizendo que conectamos ***/

  /*** configura o modo de erro para excecoes ***/
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  /*** prepara a busca ***/
  $stmt = $dbh->prepare("SELECT *FROM emprestimoLivro WHERE matriculaEstudante=:matriculaEstudante");
  $stmt->bindParam(':matriculaEstudante', $matriculaEstudante, PDO::PARAM_STR);

  $stmt->execute();

  $resultado = $stmt->fetchAll();

  echo '<table class="CSSTableGenerator" border="1">'; // inicia uma tabela no HTML

  echo "<tr><td>"."Titulo"."</td><td>"."Autor"."</td><td>"."Edicao"."</td><td>"."Data de Emprestimo"."</td><td>"."Data de Devolucao"."</td></tr>";

  foreach($resultado as $row){   //Cria um loop para ir em todos os resultados

    $stmt = $dbh->prepare("SELECT titulo, autor, edicao FROM livro WHERE ISBN=:isbn");
    $stmt->bindParam(':isbn', $row['ISBN'], PDO::PARAM_STR);

    $stmt->execute();

    $info = $stmt->fetch();

    echo "<tr><td>".$info['titulo'] ."</td><td>".$info['autor']. "</td><td>".$info['edicao']."</td><td>".$row['dataEmprestimo']."</td><td>".$row['dataDevolucao'] ."</td></tr>";
  }
  echo '</table><br><br>'; //fecha a tabela em HTML
  }
catch(Exception $e){
  /*** se estamos aqui, ocorreu algo de errado ao acessar o banco de dados ***/
  echo '<h2>Nao foi possivel processar a sua requisicao. Tente novamente mais tarde.</h2>';
}

?>
      <!-- O login e a senha do usuario sao retornados para a sua pagina de acoes -->
      <div class="form">
        <form action="login_submissao.php" method="post"> 
          <input type="hidden" id="login" name="login" value=<?php echo $loginUser; ?> maxlength="20" />
          <input type="hidden" id="senha" name="senha" value=<?php echo $senhaUser; ?> maxlength="20" />
          <button type="submit" class="button button-block"/>Voltar</button>
        </form>
      </div> <!-- form -->
    </div> <!-- formTable -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/index.js"></script>
  </body>
</html>