-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: BimManager
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dadosAdministrador`
--

DROP TABLE IF EXISTS `dadosAdministrador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dadosAdministrador` (
  `idAdmin` varchar(256) NOT NULL,
  `nome` varchar(128) NOT NULL,
  `dataNascimento` date NOT NULL,
  `CPF` varchar(64) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `sexo` enum('M','F') DEFAULT NULL,
  KEY `idAdmin` (`idAdmin`),
  CONSTRAINT `dadosAdministrador_ibfk_1` FOREIGN KEY (`idAdmin`) REFERENCES `usuario` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dadosAdministrador`
--

LOCK TABLES `dadosAdministrador` WRITE;
/*!40000 ALTER TABLE `dadosAdministrador` DISABLE KEYS */;
INSERT INTO `dadosAdministrador` VALUES ('administrador','Hikikomori Shinkai','1990-10-10','87109216621','hikikomori@gmail.com','M'),('31975','Patrick Perroni Pereira','1990-09-10','12312312345','','M');
/*!40000 ALTER TABLE `dadosAdministrador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dadosEstudante`
--

DROP TABLE IF EXISTS `dadosEstudante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dadosEstudante` (
  `nome` varchar(128) NOT NULL,
  `dataNascimento` date NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `CPF` varchar(64) NOT NULL,
  `sexo` enum('M','F') DEFAULT NULL,
  `numMatricula` varchar(16) NOT NULL,
  PRIMARY KEY (`numMatricula`),
  CONSTRAINT `dadosEstudante_ibfk_1` FOREIGN KEY (`numMatricula`) REFERENCES `usuario` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dadosEstudante`
--

LOCK TABLES `dadosEstudante` WRITE;
/*!40000 ALTER TABLE `dadosEstudante` DISABLE KEYS */;
INSERT INTO `dadosEstudante` VALUES ('Joaquim Manoel Pereira','1990-01-01','manoel@gmail.com','17117117100','M','32009');
/*!40000 ALTER TABLE `dadosEstudante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emprestimoLivro`
--

DROP TABLE IF EXISTS `emprestimoLivro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emprestimoLivro` (
  `matriculaEstudante` varchar(32) NOT NULL,
  `ISBN` varchar(16) NOT NULL,
  `dataEmprestimo` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dataDevolucao` date NOT NULL,
  KEY `matriculaEstudante` (`matriculaEstudante`),
  KEY `ISBN` (`ISBN`),
  CONSTRAINT `emprestimoLivro_ibfk_1` FOREIGN KEY (`matriculaEstudante`) REFERENCES `dadosEstudante` (`numMatricula`),
  CONSTRAINT `emprestimoLivro_ibfk_2` FOREIGN KEY (`ISBN`) REFERENCES `livro` (`ISBN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emprestimoLivro`
--

LOCK TABLES `emprestimoLivro` WRITE;
/*!40000 ALTER TABLE `emprestimoLivro` DISABLE KEYS */;
/*!40000 ALTER TABLE `emprestimoLivro` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER subQtdLivro AFTER INSERT ON emprestimoLivro FOR EACH ROW UPDATE livro SET quantidade = quantidade - 1 WHERE ISBN = NEW.ISBN */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER adQtdLivro AFTER DELETE ON emprestimoLivro FOR EACH ROW UPDATE livro SET quantidade = quantidade + 1 WHERE ISBN = OLD.ISBN */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `livro`
--

DROP TABLE IF EXISTS `livro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `livro` (
  `ISBN` varchar(16) NOT NULL,
  `titulo` varchar(64) NOT NULL,
  `genero` varchar(32) NOT NULL,
  `autor` varchar(64) NOT NULL,
  `editora` varchar(32) NOT NULL,
  `edicao` int(11) NOT NULL,
  `ano` int(11) DEFAULT NULL,
  `local` varchar(32) DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `paginas` int(11) DEFAULT NULL,
  `localizacao` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`ISBN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `livro`
--

LOCK TABLES `livro` WRITE;
/*!40000 ALTER TABLE `livro` DISABLE KEYS */;
INSERT INTO `livro` VALUES ('1111','As Brasas','Mentiroso','Dilma Rousseff','Abril',1,2000,'Itajuba',4,300,'A44');
/*!40000 ALTER TABLE `livro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `login` varchar(256) NOT NULL DEFAULT '',
  `senha` varchar(256) NOT NULL DEFAULT '',
  `tipoUsuario` enum('admin','estudante') DEFAULT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('31975','cbb7353e6d953ef360baf960c122346276c6e320','admin'),('32009','726dfbbf2d82742fc601e72b7538ea72c4fcf7b0','estudante'),('administrador','8cb2237d0679ca88db6464eac60da96345513964','admin');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-10 14:38:52
